import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {action1, action2} from './actions';
import { connect } from 'react-redux';

class App extends Component {
  render() {
  	console.log(this.props)
    return (
      <div className="App">
        <button onClick={() => {this.props.action1(); this.props.action2()}}>test</button>
      </div>
    );
  }
}

function mapStatetoProps(state) {
	return state;
}

function mapDispatchToProps(dispatch) {
	return {
		action1: () => {dispatch(action1())},
		action2: () => {dispatch(action2())}
	}
}

export default connect(mapStatetoProps, mapDispatchToProps)(App);
