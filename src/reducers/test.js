
const initialState = [{
    text: 'Use Redux',
    marked: false,
    id: 0
}];

export default function test(state = initialState, action) {
    switch (action.type) {
        case 'TEST':
            return state;

        default:
            return state;
    }
}