import { combineReducers } from 'redux';
import test from './test';
import test1 from './test1';

const rootReducer = combineReducers({
    test, test1
});

export default rootReducer;