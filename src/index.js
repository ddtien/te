import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import rootReducer from './reducers';
import { createStore, compose } from 'redux';
import { Provider } from 'react-redux'

// Create store
const store = createStore(rootReducer);

const appRoot = (
    <Provider store={store}>
        <div>
            <App />
        </div>
    </Provider>
)

ReactDOM.render(appRoot, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
